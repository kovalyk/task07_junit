package task1;

import task1.model.LongestPlateau;

public class Main{
    public static void main(String[] args) {
        LongestPlateau plateau = new LongestPlateau();
        System.out.print("The Length is ");
        System.out.println(plateau.getLength(new int[]{1, 2, 3,
                4, 4, 4, 5, 5, 6, 6, 6, 6}));
        System.out.print("The Location is ");
        System.out.println(plateau.getLocation(new int[]{1, 2, 3,
                4, 4, 4, 5, 5, 6, 6, 6, 6}));

    }
}