package task1.view;


@FunctionalInterface
public interface Printable {
    void print(int[] data);
}
