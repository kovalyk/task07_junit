package task1.model;

public class LongestPlateau {
    public int getLength(int[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException();
        }
        int length = 1;
        for (int i = 1; i < data.length; i++) {
            if (data[i - length] == data[i]) {
                length++;
            }
        }
        return length;
    }
    
    public int getLocation(int[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException();
        }
        int count = 1;
        int length = 1;
        for (int i = 1; i < data.length; i++) {
            if (data[i - 1] == data[i]) {
                count++;
                length = Math.max(length, count);
            } else {
                count = 1;
            }
        }
        return length;
    }
}
