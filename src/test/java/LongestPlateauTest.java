import org.junit.jupiter.api.Test;
import task1.model.LongestPlateau;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongestPlateauTest {
    @Test
    public void testGetLength() {
        LongestPlateau plateau = new LongestPlateau();
        assertEquals(plateau.getLength(new int[]{2, 4, 4,
                4, 4, 4, 5, 5, 6, 6, 6, 6}), 5);
        System.out.println("getLength() is correct!");
    }

    @Test
    public void testGetLocation() {
        LongestPlateau plateau = new LongestPlateau();
        assertEquals(plateau.getLocation(new int[]{2, 2, 3, 1,
                2, 3, 4, 4, 4, 5, 5, 6}), 3);
        System.out.println("getLocation() is correct!");
    }
}

